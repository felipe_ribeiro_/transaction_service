package eu.number26.transactionservice.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.api.NotFoundException;

import eu.number26.transactionservice.model.Transaction;
import eu.number26.transactionservice.service.TransactionService;

@Path("")
public class TransactionResource {
	
	TransactionService transactionService = new TransactionService();
	
	@PUT
	@Path("/transaction/{id}")
	@Consumes("application/json")
	@Produces("text/plain")
	public String addTransaction(@PathParam("id") long id, JSONObject transactionJSON) throws JSONException {
		Transaction transaction = new Transaction();		
		transaction.setId(id);
		transaction.setAmount(transactionJSON.getDouble("amount"));
		transaction.setType(transactionJSON.getString("type"));
		if (transactionJSON.has("parent_id")) {
			long parentId = transactionJSON.getLong("parent_id");
			Transaction parent = transactionService.find(parentId);
			if (parent == null) {
				throw new NotFoundException("Transaction with id "+ parentId + " not found.");
			}
			transaction.setParent(parent);
		}
		transactionService.save(transaction);
		return transaction.getId() + " added.";
	}

	@Path("/transaction/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getTransaction(@PathParam("id") long id) throws JSONException {
		Transaction transaction = transactionService.find(id);
		if (transaction == null) {
			throw new NotFoundException("Transaction with id "+ id + " not found.");
		}
		Transaction parent = transaction.getParent();
		JSONObject result = new JSONObject();
		result.put("amount", transaction.getAmount());
		result.put("type", transaction.getType());
		if (parent != null) {
			result.put("parent_id", parent.getId());
		}
		return result;
	}
	
	@Path("/types/{type}")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String listSameType(@PathParam("type") String type) throws JSONException {	
		List<Long> listSameType = transactionService.listSameType(type);
		return listSameType.toString();
	}	

	@Path("/sum/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getSum(@PathParam("id") long id) throws JSONException {		
		Transaction transaction = transactionService.find(id);
		double sum = transactionService.sumAmount(transaction);
		JSONObject result = new JSONObject();
		result.put("sum", sum);		
		return result;
	}
}
