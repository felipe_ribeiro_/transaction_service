package eu.number26.transactionservice.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Transaction {
	
	private Long id;
	
	private Double amount;
	
	private String type;
	
	private Transaction parent;
	
	private List<Transaction> children = new ArrayList<>();
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Transaction getParent() {
		return parent;
	}
	public void setParent(Transaction parent) {
		//Remove this from list of children of the old parent 
		if (this.parent != null){
			this.parent.children.remove(this);
		}
		this.parent = parent;
		//Add this in list of children of the new parent
		if (parent != null) {
			parent.children.add(this);
		}
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Transaction> getChildren() {
		return children;
	}	
}
