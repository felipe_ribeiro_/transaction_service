package eu.number26.transactionservice.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.number26.transactionservice.model.Transaction;

public class TransactionService {

	//Map simulates database
	static private Map<Long, Transaction> transactionsMap = new HashMap<>();
	
	public double sumAmount(Transaction transaction) {
		double amount = transaction.getAmount();
		for (Transaction child : transaction.getChildren()) {
			amount += sumAmount(child);
		}
		return amount;
	}
	
	public Transaction find(long id) {
		Transaction transaction = transactionsMap.get(id);		
		return transaction;
	}
	
	public Transaction save(Transaction transaction) {
		return transactionsMap.put(transaction.getId(), transaction);
	}
	
	public List<Long> listSameType(String type) {
		List<Long> list = new ArrayList<>();
		for (Transaction transaction : transactionsMap.values()) {
			if (transaction.getType().equals(type)) {
				list.add(transaction.getId());
			}
		}	
		return list;
	}
	
	
}
