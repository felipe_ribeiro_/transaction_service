package eu.number26.transactionservice.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.net.URI;
import java.util.Iterator;

import javax.ws.rs.core.UriBuilder;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.test.framework.AppDescriptor;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import com.sun.net.httpserver.HttpServer;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransactionTest extends JerseyTest {

	private static final String URL = "http://localhost:8085/transactionservice";
	private static final String RESOURCES_PACKAGE = "eu.number26.transactionservice.resources";
	
	static {
		try {
			createHttpServer().start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
    private static HttpServer createHttpServer() throws IOException {
		ResourceConfig resourceConfig = new PackagesResourceConfig(RESOURCES_PACKAGE);
        return HttpServerFactory.create(getURI(), resourceConfig);
    }
    
    private static URI getURI() {
		return UriBuilder.fromUri(URL).build();
    }
    
	@Override
	protected AppDescriptor configure() {
		return new WebAppDescriptor.Builder().build();
	}
    
    public WebResource getWebResource() {
		return client().resource(URL);
	}

	@Test public void test1_addTransaction() {		
	
		boolean thrown = false;
        JSONObject transaction = new JSONObject();
        WebResource webResource = getWebResource();
        try {        	
			transaction.put("amount", "5000").put("type", "cars");
			ClientResponse response = webResource.path("/transaction/10").type("application/json")
					.put(ClientResponse.class, transaction);
	        assertEquals(200, response.getStatus());
		} catch(Exception e) {
			e.printStackTrace();
			thrown = true;
		}		
		assertFalse(thrown);
    }

	
	@Test public void test2_addTransactionWithParent() {
		boolean thrown = false;
		
		JSONObject transaction = new JSONObject();
		WebResource webResource = getWebResource();
		try {			
			transaction.put("amount", "100.5").put("type", "shopping").put("parent_id", "10");
			ClientResponse response = webResource.path("/transaction/11").type("application/json")
					.put(ClientResponse.class, transaction);
			assertEquals(200, response.getStatus());
		} catch(Exception e) {
			e.printStackTrace();
			thrown = true;
		}		
		assertFalse(thrown);
	}
	
	
	@Test public void test3_addTransactionWithParentNotFound() {		
		boolean thrown = false;		
		JSONObject transaction = new JSONObject();
		WebResource webResource = getWebResource();
		try {
			transaction.put("amount", "100.5").put("type", "shopping").put("parent_id", "20");
			ClientResponse response = webResource.path("/transaction/12").type("application/json").put(ClientResponse.class,
					transaction);
			assertEquals(404, response.getStatus());
		} catch(Exception e) {
			e.printStackTrace();
			thrown = true;
		}		
		assertFalse(thrown);
	}
	
	@Test public void test4_getTransaction() {
		boolean thrown = false;
		WebResource webResource = getWebResource();
		try {
			JSONObject transaction = webResource.path("/transaction/11").type("application/json").get(JSONObject.class);
			JSONObject expected = new JSONObject().put("amount", 100.5).put("type", "shopping").put("parent_id", 10);
			expected.equals(transaction);			
			assertJSONEquals(expected, transaction);			
		} catch(Exception e) {
			e.printStackTrace();
			thrown = true;
		}		
		assertFalse(thrown);
	}
	
	@Test public void test5_getTransactionNotFound() {
		boolean thrown = false;
		WebResource webResource = getWebResource();	
		try {
			ClientResponse response = webResource.path("/transaction/21").type("application/json").get(ClientResponse.class);			
			assertEquals(404, response.getStatus());			
		} catch(Exception e) {
			e.printStackTrace();
			thrown = true;
		}		
		assertFalse(thrown);
	}
	
	@Test public void test6_getTypes() {
		boolean thrown = false;
		WebResource webResource = getWebResource();
		try {
			String types = webResource.path("/types/cars").type("application/json").get(String.class);			
			assertEquals("[10]", types);			
		} catch(Exception e) {
			e.printStackTrace();
			thrown = true;
		}		
		assertFalse(thrown);
	}
	
	@Test public void test7_getSum() {
		boolean thrown = false;
		WebResource webResource = getWebResource();
		try {
			JSONObject result = webResource.path("/sum/10").type("application/json").get(JSONObject.class);
			JSONObject expected = new JSONObject().put("sum", 100.5 + 5000);
			assertJSONEquals(expected, result);
		} catch(Exception e) {
			e.printStackTrace();
			thrown = true;
		}		
		assertFalse(thrown);
	}
	
	private void assertJSONEquals(JSONObject transaction, JSONObject result) throws JSONException {
		assertEquals(transaction.length(),result.length());
		Iterator<String> keys = transaction.keys();
		while (keys.hasNext()) {
		    String key = keys.next();
			assertEquals(transaction.get(key), result.get(key));
		}
	}
}
