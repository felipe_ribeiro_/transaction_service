This is a RESTful web service that stores some transactions (in memory) and returns information about those transactions.
The transactions to be stored have a type and an amount.
The service supports returning all transactions of a type.
Also, transactions can be linked to each other (using a "parent_id") and it is possible
 to know the total amount involved for all transactions linked to a particular transaction.

Instructions:

RUN:
*Import on Eclipse as Maven Project. Use Java 8.
*Run on Jetty. port=8080, Context-path = /transactionservice

RUN TESTS:
Run TransactionTest.java on JUnit Test or use the prompt command: mvn test

SOME EXAMPLES:
PUT http://localhost:8080/transactionservice/transaction/10 { "amount": 5000, "type": "cars" } 

=> { "status": "ok" }

PUT http://localhost:8080/transactionservice/transaction/11 { "amount": 10000, "type": "shopping", "parent_id": 10 } 

=> { "status": "ok" } 

GET http://localhost:8080/transactionservice/types/cars => [10] 

GET  http://localhost:8080/transactionservice/sum/10 => {"sum":15000} 

GET  http://localhost:8080/transactionservice/sum/11 => {"sum":10000}